package com.secondstack.training.spring.jpa.hibernate.controller.web;

import com.secondstack.training.spring.jpa.hibernate.domain.Student;
import com.secondstack.training.spring.jpa.hibernate.service.StudentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "student")
//@RequestMapping(value = "student", headers = {"Accept=text/html"})
public class StudentController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String form(ModelMap modelMap) {
        logger.debug("Received request to get form student");

        Student student = new Student();
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("studentUrl", "/student");
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-form-tiles";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@ModelAttribute("student") Student student, ModelMap modelMap) {
        logger.debug("Received request to create student");

        studentService.save(student);
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-data-tiles";
    }

    @RequestMapping(value = "/form", params = "id", method = RequestMethod.GET)
    public String form(@RequestParam("id") Integer id, ModelMap modelMap) {
        logger.debug("Received request to get form student");

        Student student = studentService.findById(id);
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("studentUrl", "/student?id=" + id);
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-form-tiles";
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.POST)
    public String update(@RequestParam("id") Integer id, @ModelAttribute("student") Student student, ModelMap modelMap) {
        logger.debug("Received request to update student");

        studentService.update(id, student);
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-data-tiles";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String findAll(ModelMap modelMap) {
        logger.debug("Received request to get list student");

        List<Student> studentList = studentService.findAll();
        modelMap.addAttribute("studentList", studentList);
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-list-tiles";
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    public String findById(@RequestParam("id") Integer id, ModelMap modelMap) {
        logger.debug("Received request to get data student");

        Student student = studentService.findById(id);
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("menuStudentClass", "active");

        return "student-data-tiles";
    }

    @RequestMapping(value = "/delete", params = {"id"}, method = RequestMethod.GET)
    public String delete(@RequestParam("id") Integer id, ModelMap modelMap) {
        logger.debug("Received request to delete student");

        studentService.delete(id);

        return "redirect:/student";
    }

    @RequestMapping(value = "/report", params = {"format"}, method = RequestMethod.GET)
    public String report(@RequestParam("format") String format,
                         @RequestParam(value = "isSexShow", required = false, defaultValue = "true") Boolean isSexShow,
                         ModelMap modelMap) {
        logger.debug("Received request to get report" + format + " student" + isSexShow);

        modelMap.addAttribute("datasource", studentService.findAll());
        modelMap.addAttribute("isSexShow", isSexShow);
        if ("pdf".equalsIgnoreCase(format)) {
            return "studentPdfReport";
        } else if ("xls".equalsIgnoreCase(format)) {
            return "studentXlsReport";
        } else if ("csv".equalsIgnoreCase(format)) {
            return "studentCsvReport";
        } else {
            return "studentHtmlReport";
        }
    }
}
